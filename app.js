var express = require('./config/express');

var appHost = 'localhost';
var appPort = 8080;

var app = express();
app.listen(appPort, appHost);

console.log('App running at http://' + appHost + ':' + appPort + '/');

module.exports = app;
