var expect = require("chai").expect;
var matrixUtils = require('../app/utils/matrixUtils');

var matrix4x4x4OfZeros = [[[0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0]],
  [[0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0]],
  [[0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0]],
  [[0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0]]];

var matrixWith4InX2Y2Z2 = [[[0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0]],
  [[0, 0, 0, 0], [0, 4, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0]],
  [[0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0]],
  [[0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0]]];

var updateOperationWith4InX2Y2Z2 = {"type": "update", "x": 2, "y": 2, "z": 2, "w": 4};

var queryOperationX11Y11Z11X23Y23Z23 = {"type": "query", "x1": 1, "y1": 1, "z1": 1, "x2": 3, "y2": 3, "z2": 3};

var testCase1 = {
  "size": 4,
  "operations": [
    {"type": "update", "x": 2, "y": 2, "z": 2, "w": 4},
    {"type": "query", "x1": 1, "y1": 1, "z1": 1, "x2": 3, "y2": 3, "z2": 3},
    {"type": "update", "x": 1, "y": 1, "z": 1, "w": 23},
    {"type": "query", "x1": 2, "y1": 2, "z1": 2, "x2": 4, "y2": 4, "z2": 4},
    {"type": "query", "x1": 1, "y1": 1, "z1": 1, "x2": 3, "y2": 3, "z2": 3}
  ]
};

var expectedTestCase1Result = [4, 4, 27];

var testCase2 = {
  "size": 2,
  "operations": [
    {"type": "update", "x": 2, "y": 2, "z": 2, "w": 1},
    {"type": "query", "x1": 1, "y1": 1, "z1": 1, "x2": 1, "y2": 1, "z2": 1},
    {"type": "query", "x1": 1, "y1": 1, "z1": 1, "x2": 2, "y2": 2, "z2": 2},
    {"type": "query", "x1": 2, "y1": 2, "z1": 2, "x2": 2, "y2": 2, "z2": 2}
  ]
};

var expectedTestCase2Result = [0, 1, 1];

describe('matrixUtils', function () {
  describe('buildMatrix', function () {
    it('should return a matrix 4x4x4 of zeros', function () {
      expect(matrixUtils.buildMatrix(4)).to.deep.equal(matrix4x4x4OfZeros);
    });
  });
});

describe('matrixUtils', function () {
  describe('updateMatrix', function () {
    it('should return a matrix with a 4 in x=2, y=2 and z=2', function () {
      expect(matrixUtils.updateMatrix(matrix4x4x4OfZeros, updateOperationWith4InX2Y2Z2)).to.deep.equal(
              matrixWith4InX2Y2Z2);
    });
  });
});

describe('matrixUtils', function () {
  describe('queryMatrix', function () {
    it('should return a 4', function () {
      expect(matrixUtils.queryMatrix(matrixWith4InX2Y2Z2, queryOperationX11Y11Z11X23Y23Z23)).to.equal(4);
    });
  });
});

describe('matrixUtils', function () {
  describe('executeTestCase', function () {
    it('should return the array [4, 4, 27 ]', function () {
      expect(matrixUtils.executeTestCase(testCase2)).to.deep.equal(expectedTestCase2Result);
    });
  });
});

describe('matrixUtils', function () {
  describe('executeTestCase', function () {
    it('should return the array [0, 1, 1]', function () {
      expect(matrixUtils.executeTestCase(testCase1)).to.deep.equal(expectedTestCase1Result);
    });
  });
});
