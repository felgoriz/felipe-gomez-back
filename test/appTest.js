var chai = require("chai");
var chaiHttp = require("chai-http");
var should = chai.should();
var app = require("../app");

chai.use(chaiHttp);

var testCases = [{
    "size": 4,
    "operations": [
      {"type": "update", "x": 2, "y": 2, "z": 2, "w": 4},
      {"type": "query", "x1": 1, "y1": 1, "z1": 1, "x2": 3, "y2": 3, "z2": 3},
      {"type": "update", "x": 1, "y": 1, "z": 1, "w": 23},
      {"type": "query", "x1": 2, "y1": 2, "z1": 2, "x2": 4, "y2": 4, "z2": 4},
      {"type": "query", "x1": 1, "y1": 1, "z1": 1, "x2": 3, "y2": 3, "z2": 3}
    ]
  }, {
    "size": 2,
    "operations": [
      {"type": "update", "x": 2, "y": 2, "z": 2, "w": 1},
      {"type": "query", "x1": 1, "y1": 1, "z1": 1, "x2": 1, "y2": 1, "z2": 1},
      {"type": "query", "x1": 1, "y1": 1, "z1": 1, "x2": 2, "y2": 2, "z2": 2},
      {"type": "query", "x1": 2, "y1": 2, "z1": 2, "x2": 2, "y2": 2, "z2": 2}
    ]
  }
];

var testCasesResult = [[4, 4, 27], [0, 1, 1]];

describe('app', function () {
  describe('/POST /api/matrix/test', function () {
    it('should execute the test cases and return a valid result', (done) => {
      chai.request(app)
        .post("/api/matrix/test")
        .send(testCases)
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.deep.equal(testCasesResult);
          done();
        });
    });
  });
});
