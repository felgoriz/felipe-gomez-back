var host = 'localhost';
var port = '8080';

$(document).ready(function () {
  $("#execute").click(function () {
    executeTestCases();
  });
});

function executeTestCases() {
  var input = $('#input').val();
  try {
    var testCases = buildTestCases(input);
    executeTestCasesRequest(testCases);
  } catch (err) {
    $('#output').text("Invalid input");
  }
}

function buildTestCases(input) {
  var lines = input.split("\n");
  var testCasesNumber = parseInt(lines[0]);
  var index = 1;
  var testCases = [];
  for (var i = 0; i < testCasesNumber; i++) {
    var buildTestCaseResult = buildTestCase(lines, index);
    testCases.push(buildTestCaseResult.testCase);
    index = buildTestCaseResult.index;
  }
  return testCases;
}

function buildTestCase(lines, index) {
  var definitionLine = lines[index];
  var definition = buildDefinition(definitionLine);
  index++;
  var operations = [];
  for (var i = 0; i < definition.operationsNumber; i++) {
    operations.push(buildOperation(lines[index]));
    index++;
  }
  return {"testCase": {"size": definition.size, "operations": operations}, "index": index};
}

function buildDefinition(line) {
  var elements = line.split(" ");
  var size = parseInt(elements[0]);
  var operationsNumber = parseInt(elements[1]);
  return {"size": size, "operationsNumber": operationsNumber};
}

function buildOperation(line) {
  var elements = line.split(" ");
  var type = elements[0];
  if (type === "UPDATE") {
    return buildUpdateOperation(elements);
  }
  if (type === "QUERY") {
    return buildQueryOperation(elements);
  }
}

function buildUpdateOperation(elements) {
  var x = parseInt(elements[1]);
  var y = parseInt(elements[2]);
  var z = parseInt(elements[3]);
  var w = parseInt(elements[4]);
  return {"type": "update", "x": x, "y": y, "z": z, "w": w};
}

function buildQueryOperation(elements) {
  var x1 = parseInt(elements[1]);
  var y1 = parseInt(elements[2]);
  var z1 = parseInt(elements[3]);
  var x2 = parseInt(elements[4]);
  var y2 = parseInt(elements[5]);
  var z2 = parseInt(elements[6]);
  return {"type": "query", "x1": x1, "y1": y1, "z1": z1, "x2": x2, "y2": y2, "z2": z2};
}

function buildUrl(path) {
  return 'http://' + host + ':' + port + path;
}

function executeTestCasesRequest(testCases) {
  var url = buildUrl("/api/matrix/test");
  $.ajax({
    url: url,
    type: 'POST',
    contentType: 'application/json',
    data: JSON.stringify(testCases),
    success: function (testCasesResult) {
      showOutput(testCasesResult);
    },
    error: function () {
      $('#output').text("Server error");
    }
  });
}

function showOutput(testCasesResult) {
  var output = "";
  for (var testCaseResult of testCasesResult) {
    for (var queryResult of testCaseResult) {
      output += queryResult + "\n";
    }
  }
  $('#output').text(output);
}
