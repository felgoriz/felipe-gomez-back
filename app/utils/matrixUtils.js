exports.buildMatrix = function (size) {
  var matrix = new Array();
  for (i = 0; i < size; i++) {
    matrix[i] = new Array();
    for (j = 0; j < size; j++) {
      matrix[i][j] = new Array();
      for (k = 0; k < size; k++) {
        matrix[i][j][k] = 0;
      }
    }
  }
  return matrix;
};

exports.updateMatrix = function (matrix, operation) {
  matrix[operation.x - 1][operation.y - 1][operation.z - 1] = operation.w;
  return matrix;
};

exports.queryMatrix = function (matrix, operation) {
  var sum = 0;
  for (i = operation.x1 - 1; i <= operation.x2 - 1; i++) {
    for (j = operation.y1 - 1; j <= operation.y2 - 1; j++) {
      for (k = operation.z1 - 1; k <= operation.z2 - 1; k++) {
        sum += matrix[i][j][k];
      }
    }
  }
  return sum;
};

exports.executeTestCase = function (testCase) {
  var matrix = this.buildMatrix(testCase.size);
  var result = [];
  for (var operation of testCase.operations) {
    if (operation.type === "update") {
      matrix = this.updateMatrix(matrix, operation);
    }
    if (operation.type === "query") {
      result.push(this.queryMatrix(matrix, operation));
    }
  }
  return result;
};
