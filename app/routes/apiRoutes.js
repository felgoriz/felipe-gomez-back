var api = require('../controllers/apiController');

module.exports = function(app) {
  app.route('/api/matrix/test').post(api.executeMatrixTestCases);
};
