var web = require('../controllers/webController');

module.exports = function(app) {
  app.route('/').get(web.renderApp);
};
