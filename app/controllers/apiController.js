var matrixUtils = require('../utils/matrixUtils');
exports.executeMatrixTestCases = function (req, res) {
  var testCases = req.body;
  var result = [];
  for (var testCase of testCases) {
    result.push(matrixUtils.executeTestCase(testCase));
  }
  res.json(result);
};
