var express = require('express');
var bodyParser = require('body-parser');
var ejs = require('ejs');

module.exports = function () {
  var app = express();
  app.use(bodyParser.json());
  app.set('views', './app/views');
  app.set('view engine', 'html');
  app.engine('html', ejs.renderFile);
  app.use(express.static('./public'));
  require('../app/routes/apiRoutes')(app);
  require('../app/routes/webRoutes')(app);
  return app;
};
